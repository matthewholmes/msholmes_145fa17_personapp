
import java.util.InputMismatchException;
import java.util.Scanner;

// include any necessary import statements

/**
 * Application (driver) class used to test the capabilities of class Person
 *
 * @author 
 * @version 
 */
public class PersonTest
{
    public static void main( String[] args )
    {
        // instantiate Scanner object for reading in keyboard input
        Scanner input = new Scanner( System.in );
                
        // read in the name 
        /* 
        If we are calling a static method from within the class in
        which that static method has been defined, we don't need to
        use ClassName.methodName() as the method call (we don't NEED 
        to use dot notation). But!!!! It won't hurt to do so:
        */
        String name1 = readName( input ); // local reference-type variables are null by default 
        int ssn1 = readSSN(input);
        boolean voterStatus1 = readVoterStatus( input );
        double salary1 = readAnnualSalary(input);
         
        // Create new Person object using the parameters above, and assign 
        // object reference to newly declared person1 object variable
        Person person1 = new Person( name1, ssn1, voterStatus1, salary1);
        
        // Create new Person object using the parameters given in the
        // homework instructions, and assign object reference to person2
        Person person2 = new Person("Barry Obama", 987654321, false, -123.45 );

        // Print a blank line
        System.out.println();
        
        // Display person1's information
        Person.displayInfo(person1);
        
        // Print a blank line
        System.out.println();
        
        // Display person2's information
        Person.displayInfo(person2);
        
        // update person2's information by calling each of the
        // four instance methods
        person2.setName("Barack Huessein Obama");
        person2.setSocialSecurityNumber( 123456789 );
        person2.setVoterRegistrationStatus ( true );
        person2.setAnnualSalary( 400000.00 );
        
        // Print a blank line
        System.out.println();
        
        // Print a message indicating that person2's info has been changed
        System.out.println("Person 2's information has been changed to: ");
        
        // Print a blank line
        System.out.println();
        
        // Display person2's updated information 
        Person.displayInfo(person2);
        
    } // end method main

    /*
    This method has to be defined as static because we are not
    instantiating any PersonTest objects! (Thus, the readName method
    belongs to the PersonTest class as a whole.)
    */
    private static String readName( Scanner input ) {
        // prompt user to input name
        System.out.print("Enter the first person's name: " );
        String name;
        do {
            if ( input.hasNextLine() ) {
                name = input.nextLine(); // read in the line of text
                break; // exits the otherwise infinite do-while loop
            } else {
                System.out.println("Error! Invalid name. Try Again: ");
                input.nextLine(); //discards entire line (don't assign it to anything)
                continue; // unnecessary! why?
            } // end if/else
           
            // there are no statements to skip... so...
            
        } while (true);
        
        return name;
    } // end static method readName
    
    private static int readSSN( Scanner input) {
        // prompt user to input SSN
        System.out.print("Enter the first person's 0-digit SNN (no spaces or dashes) : " );  
        
        // read in the SSN
        int ssn = 0; // intialize because local primitve-type variables have no default value
        boolean isValid = false; // more elegant than using break
        while ( !isValid ) { // "while isValid is NOT true..."
            if ( input.hasNextInt() ) { //did the user type in an int?
                ssn = input.nextInt(); // read in the int
                input.nextLine(); // doscard other entered data, if any
                isValid = true; // this allows us to exit the loop gracefully
            } else {
                System.out.println("Error! Invalide SSN. Try again: ");
                input.nextLine(); // doscard the entire line (assuming the user didn't type in an int)
            } // end if/else
        } // end while
        return ssn;
    } // end static method readSSN

    private static boolean readVoterStatus( Scanner input) {
                
        // prompt user to input voter registration status as true or false
        System.out.print("Enter the first person's voter registration status (true or false): " );
        
        // read in the voter status 
        boolean voterStatus = false; // local primitive-type variables have no default value,
                                      // so we need to assign one
        do {
            if ( input.hasNextBoolean() ) { // did the user type in a booean value?
                voterStatus = input.nextBoolean(); // read in the boolean value
                input.nextLine();
                break; // exits the otherwise infinite do-while loop
            } else {
                System.out.println("Error! Invalid registration status. Try Again: ");
                input.nextLine(); //discards entire line (don't assign it to anything)
                continue; // unnecessary! why?
            } // end if/else
           
            // there are no statements to skip... so...
            
        } while (true);
        
        return voterStatus;
    } // end static method voterStatus
    
    private static double readAnnualSalary( Scanner input ) {
                
        // read in the salary amount
        double salary = 0.0;
        
        // use exception handing for reading in a valid annual salary
        boolean isValidSalary = false; // will update to true when a valid salary has been input
        while ( !isValidSalary ) { // loop will continue to repeat until isValidSalary == true
            // prompt user to input annual salary
            System.out.print("Enter the first person's annual salary as a decimal value: " );           
            
            try {
                // inside a try statement, you'll code the statements
                // that could potentially throw an exception
                salary = input.nextDouble();
                isValidSalary = true;
            } catch( InputMismatchException e ) {
                System.out.println("Error! Please try again. ");
            } // end try statement
            
            input.nextLine(); // discard rest of line, regardless of whether
                              // an exception was thrown
        }
        return salary;
    } // end static method 
} // end class PersonTest

